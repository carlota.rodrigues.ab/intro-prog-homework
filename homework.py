a_string = 'AAPL'
b_float = 0,1
c_integer = 100
d = 'buy'
print ('i would like to', d, c_integer, 'shares in', a_string)

person_1 = 'João'
x = 10
person_2 = 'Maria'
y = 20
company_z = 'AAPL'
print(person_1, 'needs to borrow', x, 'euros from', person_2, 'in order to trade', y, 'stocks in', company_z)

x=2
y=2
print(x*y)
z=x*y
print (z)

C = 30
F = (C * 9/5) + 32
K = C + 273.15
print (F)
print (K)

def power(a,b):
    ret_val = a**b
    return ret_val

a = 2
b = 2
c = power (a,b)
print ('the value of a^b is', c)

def equal(a,b):
    ret_val = a==b
    return ret_val

a=2
b=2
c=equal(a,b)
print('a and b are', c)